﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Per_HIerarchy_Inheritance.Entity
{
    public class PersonEntity
    {
        public decimal PersonId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }
    }
}
