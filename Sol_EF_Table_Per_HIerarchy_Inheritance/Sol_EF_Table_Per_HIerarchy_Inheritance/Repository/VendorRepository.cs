﻿using Sol_EF_Table_Per_HIerarchy_Inheritance.EF;
using Sol_EF_Table_Per_HIerarchy_Inheritance.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Per_HIerarchy_Inheritance.Repository
{
    public class VendorRepository : PersonRepository
    {

        #region Constructor
        public VendorRepository() : base()
        {

        }
        #endregion 

        #region Public Method
        public async Task<IEnumerable<VendorEntity>> GetVendorData()
        {
            return await Task.Run(() => {

                var getQuery =
                    base.PersonDbEntites
                    ?.tblPersonAlls
                    ?.OfType<Vendor>()
                    ?.AsEnumerable()
                    ?.Select((leVendorObj) => new VendorEntity()
                    {
                        FirstName = leVendorObj.FirstName,
                        LastName = leVendorObj.LastName,
                        PersonId = leVendorObj.PersonId,
                        Wages = leVendorObj.Wages
                    })
                    ?.ToList();

                return getQuery;

            });
        }
        #endregion 
    }
}
