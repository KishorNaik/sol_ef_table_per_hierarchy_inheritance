﻿using Sol_EF_Table_Per_HIerarchy_Inheritance.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Per_HIerarchy_Inheritance.Repository
{
    public class PersonRepository
    {
        #region Declaration
        private PersonDBEntities personDbEnties = null;
        #endregion

        #region Constructor
        public PersonRepository()
        {
            personDbEnties = new PersonDBEntities();

            PersonDbEntites = personDbEnties;
        }
        #endregion

        #region Public Property
        public PersonDBEntities PersonDbEntites { get; set; }
        #endregion


    }
}
