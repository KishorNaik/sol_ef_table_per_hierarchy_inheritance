﻿using Sol_EF_Table_Per_HIerarchy_Inheritance.EF;
using Sol_EF_Table_Per_HIerarchy_Inheritance.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Per_HIerarchy_Inheritance.Repository
{
    public class EmployeeRepository : PersonRepository
    {

        #region Constructor
        public EmployeeRepository() : base()
        {

        }
        #endregion 

        #region Public Method
        public async Task<IEnumerable<EmployeeEntity>> GetEmployeeData()
        {
            return await Task.Run(() => {

                var getQuery =
                    base.PersonDbEntites
                    ?.tblPersonAlls
                    ?.OfType<Employee>()
                    ?.AsEnumerable()
                    ?.Select((leEmployeeObj) => new EmployeeEntity()
                    {
                        FirstName = leEmployeeObj.FirstName,
                        LastName = leEmployeeObj.LastName,
                        PersonId = leEmployeeObj.PersonId,
                        Salary = leEmployeeObj.Salary
                    })
                    ?.ToList();

                return getQuery;

            });
        }
        #endregion 
    }
}
